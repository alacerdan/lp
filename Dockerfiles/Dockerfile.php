from php:7.2.0-apache

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

WORKDIR /var/www/html

RUN apt-get update && \
    apt-get install -y \
        curl git zlib1g-dev libzip-dev 

RUN composer require phpmailer/phpmailer




