const form = document.querySelector("form#contactform")

if (form) {
    form.addEventListener("submit", function (evt) {
        evt.preventDefault();

        const url = 'sendemail.php'
        const formData = new FormData(this);
        const data = Object.fromEntries(formData);

        if (!this.checkValidity()) {
            return;
        }

        $.post(url, data, function (response) {
            console.log(response)
            if (response.ok) {
                // alert('Thanks for your contact. We will be in touch soon.');
                const alertSuccess = document.querySelector("#send-email-success");
                alertSuccess.style.display = "unset";
                setTimeout(function () {
                    alertSuccess.style.display = "none";
                }, 6000);
                form.reset();
                return;
            }
            const alertError = document.querySelector("#send-email-error");
            alertError.style.display = "unset";
            alertError.querySelector('span').innerHTML = response.msg
            setTimeout(function () {
                alertError.style.display = "none";
            }, 6000)
            // alert("Error! Try again.");
        });

    })

}
