
readmoreContainers = document.querySelectorAll(".btn-readmore");
expandAll = document.querySelector(".expanded-all");

if (readmoreContainers && expandAll){
    readmoreContainers.forEach(item => {
        item.addEventListener("click", function(el) {
            h4 = this.querySelector("h4");
            h4.textContent = h4.textContent.trim() === "-" ? "+" : "-" ;
        })
    })
    
    
    expandAll.addEventListener("click", function(e) {
        e.preventDefault();
        const anchor = this.querySelector("a");
        anchor.textContent = anchor.textContent === "EXPAND ALL" ? "COLLAPSE ALL": "EXPAND ALL";
        readmoreContainers.forEach(item => {
            $(item).collapse("toggle");
            item.click();
        });
    })
}
