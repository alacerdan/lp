<?php

require 'vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;


error_reporting(E_ALL);
ini_set('display_errors', 0);

function validateEmail($email)
{
	if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
		return "";
	}
	return "{$email}: Not a valid email";
}

if (isset($_POST)) {
	if (!isset($_POST['email'])) {
		header("Location: / ");
	}


	$name = $_POST['name'];
	$email = $_POST['email'];
	$message = $_POST['message'];
	$iam = $_POST['iam'];
	$agree = isset($_POST['agree']) ? true : false;

	$response = [];
	// Remove all illegal characters from email
	$email = filter_var($email, FILTER_SANITIZE_EMAIL);

	// Validate e-mail
	$response['msg'] = validateEmail($email);

	$company = "Playhill";
	$to = "contact@playhill.com";
	$from = "noreply@cleveradvertising.com";

	$html = "";

	$html .= "<div><h3>Contact Form</h3>";
	$html .= "<p><b>Name</b>: " . $name . "</p>";
	$html .= "<p><b>Eu sou</b>: " . $iam . "</p>";
	$html .= "<p><b>E-mail</b>: " . $email . "</p>";
	$html .= "<p><b>Message</b>: " . $message . "</p>";
	$html .= "</div>";

	$mail = new PHPMailer;
	$mail->CharSet = 'UTF-8';

	// $mail->SMTPDebug = 3;

	$mail->isSMTP();                                      	// Set mailer to use SMTP
	$mail->Host = 'smtp.gmail.com';  						// Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               	// Enable SMTP authentication
	$mail->Username = 'noreply@cleveradvertising.com';                 					// SMTP username
	$mail->Password = 'fbaegndqsakrhevq';                           		// SMTP password
	$mail->SMTPSecure = 'tls';                            	// Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;

	$mail->SetFrom($from, 'Contact Form Playhill');

	$mail->addAddress($to, $company);

	$mail->Subject  = 'Playhill';
	$mail->isHTML(true);
	$mail->Body = $html;

	header('Content-Type: application/json; charset=utf-8');
	if (!$mail->send()) {
		$response['ok'] = false;
		$response['error'] = $mail->ErrorInfo;
	} else if (!empty ($response['msg'])) {
		$response['ok'] = false;
	} else {
		$response['ok'] = true;
	}
		echo json_encode($response);
} else {
	header("Location: / ");
}
