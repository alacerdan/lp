const {parallel, src, dest, watch} = require("gulp");
const minify = require("gulp-minify");
const cleanCss = require("gulp-clean-css");
const rename = require("gulp-rename");
const concat = require("gulp-concat");

function bundleCss() {
    return src("assets/css/*.css")
        .pipe(cleanCss())
        .pipe(rename({suffix: ".min"}))
        .pipe(dest("dist/assets/css"))
}

function bundleJs() {
    return src("assets/js/*.js")
        .pipe(concat("scripts.js"))
        .pipe(minify({ext:{
            min:'.min.js'
        }}))
        .pipe(dest("dist/assets/js"))
}

function copyImages() {
    return src("assets/img/*")
    .pipe(dest("dist/assets/img"))
}


exports.dev = function() {
    watch("assets/css/**/*.css", bundleCss);
    watch("assets/js/*.js", bundleJs);
    watch("assets/img/*", copyImages);
}

exports.default = parallel([bundleCss, bundleJs, copyImages])